var gulp = require('gulp'),
	config = require('./gulp-config.js'),
	path = require('path'),
	htmlReplace = require('gulp-html-replace'),
	ngAnnotate = require('gulp-ng-annotate'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	obfuscate = require('gulp-obfuscate'),
	dependencies = require(config.dependencies),
	relativeDistPath = config.relativeDistPath,
	srcPrefix = config.srcPrefix;

function replaceSettings(obj, prefix) {
	console.log(arguments)
	return gulp.src(
		path.join(prefix, 'index.html')
	)
		.pipe(htmlReplace(obj, {
			keepBlockTags: true,
			keepUnassigned: true
		}))
		.pipe(gulp.dest(relativeDistPath));
}

gulp.task('include:developmentCompiled', ['compile:templates'], function () {
	replaceSettings({
		libs: dependencies.development.libs,
		js: dependencies.development.js,
		css: dependencies.development.css,
		templates: 'app/templates.js'
	}, srcPrefix);
});

// TODO: refactoring

gulp.task('build:js', function () {
	return gulp.src(dependencies.development.js.map(function (el) {
			return 'src/' + el;
		}))
		.pipe(ngAnnotate())
		.pipe(concat('app/app.js'))
		.pipe(uglify())
		//.pipe(obfuscate())
		.pipe(gulp.dest(relativeDistPath));
});

gulp.task('include:preview', ['compile:templates'], function () {
	return replaceSettings({
		libs: dependencies.development.libs,
		js: 'app/app.js',
		css: dependencies.development.css,
		templates: 'app/templates.js'
	}, srcPrefix);
});

gulp.task('include:development', function () {
	replaceSettings({
		libs: dependencies.development.libs,
		js: dependencies.development.js,
		css: dependencies.development.css
	}, srcPrefix);
});
