;(function () {

    angular
        .module('Isoft.Main', [])
        .config(routerConfig);

    /* @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('main', {
                url: '/',
                templateUrl: 'app/modules/main/views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'mainCtrl'
            });
    }

} ());