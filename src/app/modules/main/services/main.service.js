;(function () {

	angular
		.module('Isoft.Main')
		.factory('mainService', mainService);

	/* @ngInject */
	function mainService($http) {
		return {
			get : get
		};

		function get() {
			return $http.get(mainUrl());
		}
	}

	function mainUrl() {
		var url = '/main';

		return url;
	}

} ());