;(function () {

    angular
        .module('Isoft.Main')
        .directive('isoftAboutUs', isoftAboutUs);

    function isoftAboutUs() {
        return {
            restrict: 'E',
            templateUrl: 'app/modules/main/directives/about-us/about-us.html',
            replace: true,
            bindToController: true,
            controller: AboutUsCtrl,
            controllerAs: 'aboutUsCtrl'
        };
    }

    /* @ngInject */
    function AboutUsCtrl() {
        var vm = this;
    }

} ());
