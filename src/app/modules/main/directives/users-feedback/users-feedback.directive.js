;(function () {

    angular
        .module('Isoft.Main')
        .directive('isoftUsersFeedback', isoftUsersFeedback);

    function isoftUsersFeedback() {
        return {
            restrict: 'E',
            templateUrl: 'app/modules/main/directives/users-feedback/users-feedback.html',
            replace: true,
            bindToController: true,
            controller: UsersFeedbackCtrl,
            controllerAs: 'usersFeedbackCtrl'
        };
    }

    /* @ngInject */
    function UsersFeedbackCtrl() {
        var vm = this;
    }

} ());
