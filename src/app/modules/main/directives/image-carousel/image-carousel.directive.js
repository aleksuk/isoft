;(function () {

    angular
        .module('Isoft.Main')
        .directive('isoftImageCarousel', isoftImageCarousel);

    function isoftImageCarousel() {
        return {
            restrict: 'E',
            templateUrl: 'app/modules/main/directives/image-carousel/image-carousel.html',
            replace: true,
            bindToController: true,
            controller: ImageCarouselCtrl,
            controllerAs: 'imageCarouselCtrl'
        };
    }

    /* @ngInject */
    function ImageCarouselCtrl() {
        var vm = this;
    }

} ());
