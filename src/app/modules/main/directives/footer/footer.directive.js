;(function () {

    angular
        .module('Isoft.Main')
        .directive('isoftFooter', isoftFooter);

    function isoftFooter() {
        return {
            restrict: 'E',
            templateUrl: 'app/modules/main/directives/footer/footer.html',
            replace: true,
            bindToController: true,
            controller: FooterCtrl,
            controllerAs: 'footerCtrl'
        };
    }

    /* @ngInject */
    function FooterCtrl() {
        var vm = this;
    }

} ());
