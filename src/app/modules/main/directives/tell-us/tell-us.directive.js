;(function () {

    angular
        .module('Isoft.Main')
        .directive('isoftTellUs', isoftTellUs);

    function isoftTellUs() {
        return {
            restrict: 'E',
            templateUrl: 'app/modules/main/directives/tell-us/tell-us.html',
            replace: true
        };
    }

} ());
