;(function () {

	angular
		.module('Isoft.Main')
		.directive('isoftHeader', isoftHeader);

	function isoftHeader() {
		return {
			restrict: 'E',
			templateUrl: 'app/modules/main/directives/header/header.html',
			replace: true,
			bindToController: true,
			controller: HeaderCtrl,
			controllerAs: 'headerCtrl'
		};
	}

	/* @ngInject */
	function HeaderCtrl() {
		var vm = this;
	}
	
} ());
