;(function () {

    angular
        .module('Isoft.Main')
        .directive('isoftServices', isoftServices);

    function isoftServices() {
        return {
            restrict: 'E',
            templateUrl: 'app/modules/main/directives/isoft-services/isoft-services.html',
            replace: true,
            bindToController: true,
            controller: IsoftServicesCtrl,
            controllerAs: 'isoftServicesCtrl'
        };
    }

    /* @ngInject */
    function IsoftServicesCtrl() {
        var vm = this;
    }

} ());
