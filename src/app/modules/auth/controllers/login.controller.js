;(function () {

	angular
		.module('Isoft.Auth')
		.controller('LoginCtrl', LoginCtrl);

	/* @ngInject */
	function LoginCtrl($state, $element, authService) {
		var vm = this;

		vm.validationResult = {
			isValid: false
		};

		vm.data = {
			userName: 'ads@sd.sd',
			password: 'ads@sd.sd'
		};

		vm.logIn = logIn;

		function logIn(e) {
			e.preventDefault();

			if (vm.validationResult.isValid) {
				$state.go('main');
			}
		}
	}

} ());
