;(function () {

	angular
		.module('Isoft.Auth')
		.factory('authService', authService);

	/* @ngInject */
	function authService($http) {
		return {
			logIn: logIn,
			logOut: logOut,
			register: register
		};

		function logIn(data) {

		}

		function logOut(data) {

		}

		function register(data) {

		}
	}

	function authUrl(id) {
		var url = '/auth';

		return (id) ? url + '/' + id : url;
	}

} ());