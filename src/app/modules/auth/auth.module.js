;(function () {

	angular
		.module('Isoft.Auth', [
			'Isoft.Validation'
		])
		.config(routerConfig);

	/* @ngInject */
	function routerConfig($stateProvider) {
		$stateProvider
			.state('login', {
				url: '/login',
				templateUrl: 'app/modules/auth/views/login.html',
				controller: 'LoginCtrl',
				controllerAs: 'loginCtrl'
			})
			.state('register', {
				url: '/register',
				templateUrl: 'app/modules/auth/views/register.html',
				controller: 'RegisterCtrl',
				controllerAs: 'registerCtrl'
			});
	}

} ());