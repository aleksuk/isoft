;(function () {

    angular
        .module('Isoft.Validation', [])
        .directive('isoftFormValidation', isoftFormValidation);

    function isoftFormValidation() {
        return {
            scope: {
                isoftFormValidation: '='
            },
            restrict: 'A',
            bindToController: true,
            controller: FormValidationCtrl,
            controllerAs: 'formValidationCtrl'
        };
    }

    /* @ngInject */
    function FormValidationCtrl($element, $scope) {
        var vm = this,
            parsley = $element.parsley();

        parsley.on('form:success', successValidation);
        parsley.on('form:invalid', failedValidation);
        $scope.$on('$destroy', destroy);

        function successValidation() {
            vm.isoftFormValidation.isValid = true;
        }

        function failedValidation() {
            vm.isoftFormValidation.isValid = false;
        }

        function destroy() {
            parsley.destroy();
        }
    }

} ());
